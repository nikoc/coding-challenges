/* 1. Challenge
Create a function called difference that takes 2 prices and calculates the difference between the 2. Validate that your function is working by passing different numbers to it.*/
/*
var difference = function (a, b) {return a - b}

difference(2,3);
 */





/* Challenge 2:
Create 2 objects for 2 different cars, set a different brand, model, plate number, color and price for each. Then change the price of the first car and the brand and model of the second car. Verify that your changes have taken place.

car1 = {
    brand: "Ferrari",
    model: "Carrera",
    plateNumber: "HH-12345-67",
    color: "red",
    price: 52000,
  }
  
  car2 = {
    brand: "Jeep",
    model: "Crosslands",
    plateNumber: "B-6789-00",
    color: "red",
    price: 32000,
  }
  
  car1.price = 55000;
  
  car2.brand = "Tesla";
  car2.model = "Model 3";
  
*/






/* Challenge 3:
Create a speed variable and give it a number value. The speed limit is 50 (Km/h). If the speed is over the limit, give the driver a speeding ticket, otherwise they're free to go.

var speed = 60;

ticket = function(speed) {
  if (speed <= 50) {
  console.log("No ticket for you!")
}
else {
  console.log("Dude, you're not supposed to speed. That's a ticket for you. May this teach you a lesson.")
 }
};

ticket(speed);
*/





/* Challenge 4:
Create a simple pool game (snooker), where the ball that's going in the hole will score 1 point only if

1. either the ball has a color
2. or all other balls went through already 

var score = 0;

var hasColor = true;
var noBallsLeft = false;

var snooker = function(hasColor,noBallsLeft) {
  if (hasColor == true || noBallsLeft == true) {
    score++;
    console.log("Strike, baby!")
    console.log("Your total score is: " + score)
  } else {
    console.log("That's a NOPE!")
    console.log("Your total score is: " + score)
  }
}

snooker(hasColor, noBallsLeft);
*/





/*Challenge 5:
Create a bowling game where an array of scores for a single player is passed to a function which returns the sum of all scores. 
However, if a score is 10 (strike), increase the total by 15 points.


scores = [8, 8, 5, 0, 5, 9, 10, 8];

total = 0;

pointsCalculator = function (scores) {
  for (score in scores) {
  total = total + scores[score]
    if (scores[score] == 10) {
      total = total + 15
    }
  console.log(total)
 }
}

pointsCalculator(scores);
*/




/* Challenge 6:
Set the constants city and color to your likings and the constant birth to your year of birth, then use them to log the following phrase as a string:
Hello, I'm name, I come from city, my favourite color is color and I'm years old. (Where years should be a calculated inline 
based on your year of birth). 

const name = "Nikola";
const city = "Hamburg";
const color = "red";
const yearOfBirth = 1990;

calculateAge = function (yearOfBirth) {
     age = 2019 - yearOfBirth
     return age
}

calculateAge(yearOfBirth);

console.log(`Hello, I'm ${name}, I come from ${city}, my favourite color is ${color} and I'm ${age} years old`)
*/








/*Challenge
Create a market. In the market, create 3 cash registers and 7 customers, each customer has a shopping cart with any amount of products inside from 1 to 10, 
products are different from each other and a customer may have more than 1 of the same product.
The 7 customers will go to the 3 cash registers more or less in an equal split and each register should calculate the total amount each customer should pay.
As there is currently a promotion, any product that costs more than 35 will get a 10% discount and any shopping cart which total is more than 100 
will get an additional 10% discount. Display the total of the carts with the $ sign preceding the value.*/


/*let cashRegister1 = function(item, amount) {
  for (item in cart1[0].price) {
    item
  }
  totalSum = item * amount 
  if (totalSum > 100) {
    totalSum = totalSum * 0.9
  } 
  return totalSum
} 


cart1 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 2},
  {name: "apple", price: 1, amount: 2}
]

cart2 = [
  {name: "applepie", price: 8, amount: 1},
  {name: "chicken wings", price: 10, amount: 10},
  {name: "mango", price: 5, amount: 4},
  {name: "chocolate", price: 2, amount: 2}
]

cart3 = [
  {name: "shrimps", price: 8, amount: 5},
  {name: "chilli", price: 1, amount: 10},
  {name: "coconut", price: 3, amount: 2},
  {name: "banana", price: 2, amount: 2}
]

cart4 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 2},
  {name: "apple", price: 1, amount: 2}
]

cart5 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 2},
  {name: "apple", price: 1, amount: 2}
]

cart6 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 2},
  {name: "apple", price: 1, amount: 2}
]

cart7 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 2},
  {name: "apple", price: 1, amount: 2}
]

cashRegister1(cart1[0].price, cart1[0].amount);

console.log(totalSum); */

// This is a cart with the item name, price and amount of each item
cart1 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 35, amount: 2},
  {name: "durian", price: 8, amount: 3},
  {name: "apple", price: 1, amount: 4}
]

// Mapping out the prices of each item and returning an array of prices only
const itemPrices = cart1.map((cart1) => {
    return cart1.price 
})

// Mapping out the amounts of each item and returning an array of amounts only
const itemAmounts = cart1.map((cart1) => {
  return cart1.amount
})

// Looping through all the prices and amount and calculating the sum of the cart


var sum = 0;
let sumCalculator = function (itemPrice, itemAmounts){
for( var i = 0; i < itemPrices.length; i++ ) {
    sum += itemPrices[i] * itemAmounts[i];
} if ( sum > 100 ) {
    sum = sum * 0.9
}
}

console.log("Dear customer, your total sum is " + sum + ". Please go to Cash register No. " + Math.floor((Math.random() * 3) + 1));
//cashRegister1(itemAmounts, itemPrices);

/*
items = [
   {name: "apple", price: 3, barcode: 1},
   {name: "banana", price: 4},
   {name: "juice", price: 5},
   {name: "beef fillet", price: 35}
]

cartA = [];
userChoice = 0
userChoice = prompt("Enter the corresponding number:")
while (cartA[i] > 5) {
 userChoice = prompt("Enter the corresponding number:")
    if (userChoice = 1) {
      cartA.push(3)
    } else if (userChoice = 2) {
      cartA.push(4);
    } else if (userChoice = 3) {
      cartA.push(5)
    } else if (userChoice = 4) {
      cartA.push(35)
    } else {
      alert("Nope, that was wrong")
    }
  }

console.log(userChoice);

let cashRegister1 = () => {
  sumTotal = item.price * cart.amount
  return sumTotal
}
let cashRegister2 = () => {}
let cashRegister3 = () => {}

cart1 = (items*2); 


console.log(items[0].price)
*/